//#include <FreeRTOS.h> // nao precisa pois o codigo do ESP32 já é portado e suporta o freertos 
#include <WiFi.h>
#include <PubSubClient.h>

const char* ssid = "ssid";
const char* password = "senha";

const char* mqtt_server = "broker.hivemq.com";

void taskbluetooth1(void *pvParameters);
void taskbluetooth2(void *pvParameters);

WiFiClient espClient;
PubSubClient client(espClient);

#define LED_PIN 2

void callback(char* topic, byte* message, unsigned int length) {
  String messageTemp;
  for (int i = 0; i < length; i++) {
    messageTemp += (char)message[i];
  }

  if (String(topic) == "esp32/controle_led_sub") {
    if(messageTemp == "on"){
      digitalWrite(LED_PIN, HIGH);
    }
    else if(messageTemp == "off"){
      digitalWrite(LED_PIN, LOW);
    }
  }
}

void setup_wifi() {
  delay(10);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void reconnect() {
  while (!client.connected()) {
    // Tentando conectar
    if (client.connect("arduinoClient")) {
      // Quando conectar, publica
      client.publish("esp32/controle_led_pub","Olá, esperando comando!");
      // ... e se inscreve novamente
      client.subscribe("esp32/controle_led_sub");
    } else {
      delay(5000);
    }
  }
}

void setup() {
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  xTaskCreatePinnedToCore(taskbluetooth1, "taskbluetooth1",  1024,  NULL,  1,  NULL,  1); // INICIALIZAÇÃO DAS TASKS
  xTaskCreatePinnedToCore(taskbluetooth2, "taskbluetooth2",  1024,  NULL,  2,  NULL,  1);
  
  Serial.begin(9600);
  Serial2.begin(9600);

  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.publish("esp32/controle_led_pub","Olá, esperando comando!");
  client.loop();
  delay(5000); //PROBLEMA: PRECISO DESSES 5 SEGUNDOS PARA PUBLICAR NO BROKER.
}

/* SOLUÇÃO: RODAR TUDO EM PARALELO, UTILIZANDO TASKS, PARA ISSO FOI IMPLEMENTADO O FREERTOS */

void taskbluetooth1(void *pvParameters){
  char bt1_cmd;
  for(;;){
    bt1_cmd =  Serial.read();
    
    if(bt1_cmd == 'a') digitalWrite(LED_PIN, HIGH);
    if(bt1_cmd == 'A') digitalWrite(LED_PIN, LOW);
    vTaskDelay(50); 
  }
  
}

void taskbluetooth2(void *pvParameters){
  char bt2_cmd;
  for(;;){
    
    bt2_cmd  =  Serial2.read();
      
    if(bt2_cmd == 'a') digitalWrite(LED_PIN, HIGH);
    if(bt2_cmd == 'A') digitalWrite(LED_PIN, LOW);
    
    vTaskDelay(50); 
  }
}
