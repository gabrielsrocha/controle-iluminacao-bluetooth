#include <WiFi.h>
#include <PubSubClient.h>

const char* ssid = "ssid";
const char* password = "senha";

const char* mqtt_server = "broker.hivemq.com";

char bt1_cmd, bt2_cmd;

WiFiClient espClient;
PubSubClient client(espClient);

#define LED_PIN 2

void callback(char* topic, byte* message, unsigned int length) {
  String messageTemp;
  for (int i = 0; i < length; i++) {
    messageTemp += (char)message[i];
  }

  if (String(topic) == "esp32/controle_led_sub") {
    if(messageTemp == "on"){
      digitalWrite(LED_PIN, HIGH);
    }
    else if(messageTemp == "off"){
      digitalWrite(LED_PIN, LOW);
    }
  }
}

void setup_wifi() {
  delay(10);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void reconnect() {
  while (!client.connected()) {
    // Tentando conectar
    if (client.connect("arduinoClient")) {
      // Quando conectar, publica
      client.publish("esp32/controle_led_pub","Olá, esperando comando!");
      // ... e se inscreve novamente
      client.subscribe("esp32/controle_led_sub");
    } else {
      delay(5000);
    }
  }
}

void setup() {
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  
  Serial.begin(9600);
  Serial2.begin(9600);

  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  bt1_cmd =  Serial.read();
  
  if(bt1_cmd == 'a') digitalWrite(LED_PIN, HIGH);
  if(bt1_cmd == 'A') digitalWrite(LED_PIN, LOW);
  vTaskDelay(50); 
  
  bt2_cmd  =  Serial2.read();
      
  if(bt2_cmd == 'a') digitalWrite(LED_PIN, HIGH);
  if(bt2_cmd == 'A') digitalWrite(LED_PIN, LOW);
  
  vTaskDelay(50); 

  /* PROBLEMA, O CODIGO SÓ IRÁ RETORNAR AO INICIO DO LOOP APÓS 5 SEGUNDOS, POIS NECESSITAMOS DESSE TEMPO PARA PUBLICAR NO MQTT */
  /* DESSA FORMA, O CÓDIGO SÓ IRÁ FAZER A LEITURA DOS MÓDULOS BLUETOOTH A CADA 5 SEGUNDOS TAMBÉM */
  if (!client.connected()) {
    reconnect();
  }
  client.publish("esp32/controle_led_pub","Olá, esperando comando!");
  client.loop();
  delay(5000); //PROBLEMA: PRECISO DESSES 5 SEGUNDOS PARA PUBLICAR NO BROKER.
}
